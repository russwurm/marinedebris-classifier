
from data.refined_floatingobjects import RefinedFlobsDataset
import torch
from datetime import datetime
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from transforms import get_train_transform
from pytorch_lightning.loggers import WandbLogger
import argparse
from model.classifier import Classifier

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', type=str, default="/media/data/marinedebris_refined")
    parser.add_argument('--model', type=str, default="resnet18")
    parser.add_argument('--imagesize', type=int, default=64)
    parser.add_argument('--crop-size', type=int, default=32)
    parser.add_argument('--workers', type=int, default=16)
    parser.add_argument('--resume-from', type=str, default=None)
    parser.add_argument('--max-epochs', type=int, default=1000)
    parser.add_argument('--batch-size', type=int, default=128)

    return parser.parse_args()

def main(args):

    imagesize = args.imagesize # in pixel
    crop_size = args.crop_size
    workers = args.workers
    model = args.model
    checkpoint = args.resume_from

    ds = RefinedFlobsDataset(root=args.data_path, fold="train", shuffle=True,
                             imagesize=imagesize * 10, transform=get_train_transform(crop_size=crop_size))

    val_ds = RefinedFlobsDataset(root=args.data_path, fold="val", shuffle=True,
                             imagesize=crop_size * 10, transform=None)

    ts = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    run_name = f"{model}_{ts}"
    logger = WandbLogger(project="marinedebris-classification", name=run_name, log_model=True, save_code=False)

    checkpointer = ModelCheckpoint(
        dirpath=f"checkpoints/{run_name}",
        filename="{epoch}-{val_accuracy:.2f}",
        monitor="val_accuracy",
        mode="max",
        save_last=True,
    )

    model = Classifier(model=model)

    train_loader = torch.utils.data.DataLoader(ds, batch_size=args.batch_size, num_workers=workers, drop_last=True)
    val_loader = torch.utils.data.DataLoader(val_ds, batch_size=args.batch_size, num_workers=workers, drop_last=True)

    trainer = pl.Trainer(max_epochs=args.max_epochs, accelerator="gpu", callbacks=[checkpointer],
                         logger=logger, fast_dev_run=False, resume_from_checkpoint=checkpoint)
    trainer.fit(model, train_dataloaders=train_loader, val_dataloaders=val_loader)

if __name__ == '__main__':
    main(parse_args())

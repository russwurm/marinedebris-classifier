# Marinedebris Classifier

<img src="doc/accra_images.png" width=100%>

## Setup

create a python environment (with virtualenv)
```
python -m venv .venv
source activate .venv/bin/activate
```

```bash
pip install -r requirements.txt
```

## Training
resume training from previously saved model
```
python train.py --data-path /media/data/marinedebris_refined --resume-from checkpoints/resnet18_2022-10-17_19:05:39/epoch=183-val_accuracy=0.96.ckpt
```
